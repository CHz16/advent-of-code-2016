Day 3: Squares With Three Sides
===============================

http://adventofcode.com/2016/day/3

Given a list of integer triples, check if they possibly represent the lengths of the sides of a triangle (the sum of any two is greater than the third). Second part transposes the order and has you read down columns instead of rows.

* Part 1: 48th place (3:20)
* Part 2: 32nd place (7:01)
