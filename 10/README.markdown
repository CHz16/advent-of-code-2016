Day 10: Balance Bots
====================

http://adventofcode.com/2016/day/10

Read a list of instructions that either store a value with a robot or tell the robot to compare its two values and give the low and high values to a specified robot or output. The tricky part is that the order of instructions in the file is **not** the order you need to execute them; stores and transfers appear in a random(?) order.

I solved it by going through every instruction first, loading up every bot with its initial values and storing every transfer instruction in a dictionary keyed by bot. Then, I find a bot holding on to two values, execute its transfer instruction, and if that results in another bot holding on to two chips, I execute THAT bot's transfer instruction, and so on.

* Part 1: 85th place (29:48)
* Part 2: 84th place (32:48)
