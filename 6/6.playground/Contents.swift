//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "6", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

var counts: [[Character: Int]] = []

input.enumerateLines { (line, stop) in
    if counts.isEmpty {
        for _ in 0..<line.count {
            counts.append([:])
        }
    }

    for (i, char) in line.enumerated() {
        let count = counts[i][char] ?? 0
        counts[i][char] = count + 1
    }
}

var part1Message = ""
var part2Message = ""
char: for c in counts {
    let maxCount = c.values.max()!
    let minCount = c.values.min()!
    for (char, count) in c {
        if count == maxCount {
            part1Message += String(char)
        }
        if count == minCount {
            part2Message += String(char)
        }
    }
}
part1Message
part2Message
