//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "1", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


enum Heading {
    case north, south, east, west

    func newHeading(turning: Character) -> Heading {
        if turning == "L" {
            switch self {
            case .north:
                return .west
            case .west:
                return .south
            case .south:
                return .east
            case .east:
                return .north
            }
        } else {
            switch self {
            case .north:
                return .east
            case .east:
                return .south
            case .south:
                return .west
            case .west:
                return .north
            }
        }
    }
}


// Part 1

var horizontalDisplacement = 0
var verticalDisplacement = 0
var heading = Heading.north

input.enumerateLines { (line, stop) in
    heading = heading.newHeading(turning: line.first!)
    let distance = Int(String(line.dropFirst()))!

    switch heading {
    case .north:
        verticalDisplacement += distance
    case .south:
        verticalDisplacement -= distance
    case .east:
        horizontalDisplacement += distance
    case .west:
        horizontalDisplacement -= distance
    }
}

abs(horizontalDisplacement) + abs(verticalDisplacement)


// Part 2

var visited: [Int: [Int: Bool]] = [:]
horizontalDisplacement = 0
verticalDisplacement = 0
heading = Heading.north

input.enumerateLines { (line, stop) in
    heading = heading.newHeading(turning: line.first!)
    let distance = Int(String(line.dropFirst()))!

    var horizontalDelta = 0, verticalDelta = 0
    switch heading {
    case .north:
        verticalDelta = 1
    case .south:
        verticalDelta = -1
    case .east:
        horizontalDelta = 1
    case .west:
        horizontalDelta = -1
    }

    let visitedPoints: [(Int, Int)] = (1...distance).map { (horizontalDisplacement + $0 * horizontalDelta, verticalDisplacement + $0 * verticalDelta) }
    for point in visitedPoints {
        (horizontalDisplacement, verticalDisplacement) = point

        if visited[horizontalDisplacement] == nil {
            visited[horizontalDisplacement] = [:]
        }

        if visited[horizontalDisplacement]?[verticalDisplacement] != nil {
            stop = true
            break
        } else {
            visited[horizontalDisplacement]?[verticalDisplacement] = true
        }
    }
}

abs(horizontalDisplacement) + abs(verticalDisplacement)
