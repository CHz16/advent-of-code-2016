#!/usr/bin/env xcrun swift -F .. -framework CryptoSwift

import CryptoSwift

let input = "uqwqemis"


// Part 2

var n = 16734552
var count = 3
var password = ["6", "9", "-", "1", "-", "-", "-", "-"]

let lookup: [Character: Int] = ["2": 2, "4": 4, "5": 5, "6": 6, "7": 7]

trial: while count < 8 {
    let trialString = input + String(n)
    let hash = trialString.md5()
    n += 1

    var c = 0
    for char in hash {
        if char != "0" {
            break
        }

        c += 1
        if c == 5 {
            let index = hash.index(hash.startIndex, offsetBy: 5)
            guard let i = lookup[hash[index]] else {
                continue trial
            }

            if password[i] != "-" {
                continue trial
            }

            password[i] = String(hash[hash.index(index, offsetBy: 1)])
            count += 1

            print(n - 1, hash)
            print(password.joined(separator: ""))

            continue trial
        }
    }
}
