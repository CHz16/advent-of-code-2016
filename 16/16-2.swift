#!/usr/bin/env xcrun swift


let input = "10111100110001111"


func fromString(_ s: String) -> [Bool] {
    var a: [Bool] = []
    for c in s {
        if c == "0" {
            a.append(false)
        } else {
            a.append(true)
        }
    }
    return a
}

func toString(_ a: [Bool]) -> String {
    var s = ""
    for b in a {
        if b {
            s.append("1")
        } else {
            s.append("0")
        }
    }
    return s
}


func randomData(fromSeed seed: [Bool], ofSize size: Int) -> [Bool] {
    if seed.count > size {
        return Array<Bool>(seed[0..<size])
    }

    var newA = seed
    newA.append(false)
    for b in seed.reversed() {
        if b {
            newA.append(false)
        } else {
            newA.append(true)
        }
    }
    return randomData(fromSeed: newA, ofSize: size)
}

func checksum(_ a: [Bool]) -> [Bool] {
    if a.count % 2 == 1 {
        return a
    }

    var newA: [Bool] = []
    var i = 0
    while i < a.count {
        if a[i] == a[i + 1] {
            newA.append(true)
        } else {
            newA.append(false)
        }
        i += 2
    }
    return checksum(newA)
}


// Part 1
print(toString(checksum(randomData(fromSeed: fromString(input), ofSize: 272))))

// Part 2
print(toString(checksum(randomData(fromSeed: fromString(input), ofSize: 35651584))))
