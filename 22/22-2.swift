#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


enum Node { case wall, movable, free }

struct State: CustomStringConvertible {
    let grid: [[Node]]
    let goalX, goalY: Int
    let freeX, freeY: Int
    let steps: Int

    var costEstimate: Int {
        // Broken up because swiftc complains about the expression being too complex
        let xEstimate = goalX + abs(goalX - freeX)
        let yEstimate = goalY * 999 + abs(goalY - freeY)
        return steps + xEstimate + yEstimate - 1
    }

    var description: String {
        return "\(goalX) \(goalY) \(freeX) \(freeY)"
    }
}


let xCount = 33, yCount = 31

var startingGrid: [[Node]] = Array(repeating: Array(repeating: .wall, count: yCount), count: xCount)
var freeX = 0, freeY = 0
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let x = Int(components[0])!
    let y = Int(components[1])!
    let used = Int(components[3])!
    let available = Int(components[4])!

    if used == 0 {
        freeX = x
        freeY = y
        startingGrid[x][y] = .free
     } else if used <= 88 {
        startingGrid[x][y] = .movable
    }
}


var visited = Set<String>()
var closestScore = Int.max

var queue = [State(grid: startingGrid, goalX: xCount - 1, goalY: 0, freeX: freeX, freeY: freeY, steps: 0)]
while !queue.isEmpty {
    let state = queue.removeFirst()
    if visited.contains(state.description) {
        continue
    }
    visited.insert(state.description)
    if state.goalX == 0 && state.goalY == 0 {
        print(state.steps)
        break
    }

    if state.goalX + state.goalY < closestScore {
        closestScore = state.goalX + state.goalY
        print(state.goalX, state.goalY, queue.count)
    }

    var newStates: [State] = []
    let candidates = [(state.freeX - 1, state.freeY), (state.freeX + 1, state.freeY), (state.freeX, state.freeY - 1), (state.freeX, state.freeY + 1)]
    for (x, y) in candidates {
        if x < 0 || x >= xCount || y < 0 || y >= yCount {
            continue
        }
        if state.grid[x][y] == .wall {
            continue
        }

        let nextGoalX, nextGoalY: Int
        if x == state.goalX && y == state.goalY {
            nextGoalX = state.freeX
            nextGoalY = state.freeY
        } else {
            nextGoalX = state.goalX
            nextGoalY = state.goalY
        }

        var nextGrid = state.grid
        nextGrid[x][y] = .free
        nextGrid[state.freeX][state.freeY] = .movable

        newStates.append(State(grid: nextGrid, goalX: nextGoalX, goalY: nextGoalY, freeX: x, freeY: y, steps: state.steps + 1))
    }
    queue += newStates
    queue = queue.sorted { $0.costEstimate < $1.costEstimate }
}
