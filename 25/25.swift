#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "25.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


struct VM {
    typealias Register = String
    enum Value {
        case literal(Int), register(Register)

        init(_ x: String) {
            if let x = Int(x) {
                self = .literal(x)
            } else {
                self = .register(x)
            }
        }
    }

    enum Instruction {
        case copy(Value, Value), increment(Value), decrement(Value), jumpNotZero(Value, Value), toggle(Value), nop, multiply(Value, Value), out(Value)
    }


    var registers: [String: Int]
    var code: [Instruction]
    var pc = 0

    init(assembly: String, registers: [String: Int]) {
        self.registers = registers

        code = []
        for line in assembly.components(separatedBy: "\n") {
            if line == " " {
                continue
            }

            let tokens = line.components(separatedBy: " ")
            switch tokens[0] {
            case "cpy":
                code.append(.copy(Value(tokens[1]), Value(tokens[2])))
            case "inc":
                code.append(.increment(Value(tokens[1])))
            case "dec":
                code.append(.decrement(Value(tokens[1])))
            case "mul":
                code.append(.multiply(Value(tokens[1]), Value(tokens[2])))
            case "jnz":
                code.append(.jumpNotZero(Value(tokens[1]), Value(tokens[2])))
            case "tgl":
                code.append(.toggle(Value(tokens[1])))
            case "nop":
                code.append(.nop)
            case "out":
                code.append(.out(Value(tokens[1])))
            default:
                break
            }
        }
    }

    func evaluate(value: Value) -> Int {
        switch value {
        case let .literal(n):
            return n
        case let .register(r):
            return registers[r]!
        }
    }


    mutating func run() {
        while pc < code.count {
            let instruction = code[pc]
            pc += 1

            switch instruction {
            case let .copy(value, destination):
                if case let .register(r) = destination {
                    registers[r] = evaluate(value: value)
                }
            case let .increment(destination):
                if case let .register(r) = destination {
                    registers[r] = registers[r]! + 1
                }
            case let .decrement(destination):
                if case let .register(r) = destination {
                    registers[r] = registers[r]! - 1
                }
            case let .multiply(value, destination):
                if case let .register(r) = destination {
                    registers[r] = registers[r]! * evaluate(value: value)
                }
            case let .jumpNotZero(value, offset):
                if evaluate(value: value) != 0 {
                    pc += evaluate(value: offset) - 1
                }
            case let .toggle(value):
                let target = pc + evaluate(value: value) - 1
                if target < 0 || target >= code.count {
                    continue
                }
                switch code[target] {
                case let .increment(destination):
                    code[target] = .decrement(destination)
                case let .decrement(destination), let .toggle(destination):
                    code[target] = .increment(destination)
                case let .jumpNotZero(value, offset):
                    code[target] = .copy(value, offset)
                case let .copy(value, destination):
                    code[target] = .jumpNotZero(value, destination)
                case .nop, .multiply, .out:
                    break
                }
            case .nop:
                break
            case let .out(value):
                print(evaluate(value: value), registers)
            }
        }
    }

}


// Part 1

var vm = VM(assembly: input, registers: ["a": 198, "b": 0, "c": 0, "d": 0])
vm.run()
print(vm.registers)
