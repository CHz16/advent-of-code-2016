#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "7.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Part 1

func containsABBA(_ sequence: String) -> Bool {
    var cursor = sequence.startIndex
    let end = sequence.index(sequence.endIndex, offsetBy: -3)

    while cursor != end {
        let a = sequence[cursor]
        let b = sequence[sequence.index(cursor, offsetBy: 1)]
        let c = sequence[sequence.index(cursor, offsetBy: 2)]
        let d = sequence[sequence.index(cursor, offsetBy: 3)]
        if a == d && b == c && a != b {
            return true
        }

        cursor = sequence.index(after: cursor)
    }

    return false
}

func supportsTLS(_ ip: String) -> Bool {
    let sequences = ip.components(separatedBy: CharacterSet(charactersIn: "[]"))

    var valid = false
    for (index, sequence) in sequences.enumerated() {
        // Skip checking the sequences outside of brackets if we've already found an ABBA.
        if index % 2 == 0 && valid {
            continue
        }

        let c = containsABBA(sequence)
        if index % 2 == 1 && c {
            return false
        } else if index % 2 == 0 && c {
            valid = c
        }
    }

    return valid
}

var validIPs = 0
input.enumerateLines { (line, stop) in
    if supportsTLS(line) {
        validIPs += 1
    }
}
print(validIPs)


// Part 2

func extractBABs(_ sequence: String) -> Set<String> {
    var babs = Set<String>()

    var cursor = sequence.startIndex
    let end = sequence.index(sequence.endIndex, offsetBy: -2)
    while cursor != end {
        let a = sequence[cursor]
        let b = sequence[sequence.index(cursor, offsetBy: 1)]
        let c = sequence[sequence.index(cursor, offsetBy: 2)]

        if a == c && a != b {
            babs.insert(String(b) + String(a) + String(b))
        }

        cursor = sequence.index(after: cursor)
    }

    return babs
}

func supportsSSL(_ ip: String) -> Bool {
    let sequences = ip.components(separatedBy: CharacterSet(charactersIn: "[]"))

    var babs = Set<String>()
    var i = 0
    while i < sequences.count {
        babs.formUnion(extractBABs(sequences[i]))
        i += 2
    }

    i = 1
    while i < sequences.count {
        for bab in babs {
            if sequences[i].contains(bab) {
                return true
            }
        }

        i += 2
    }
    
    return false
}

validIPs = 0
input.enumerateLines { (line, stop) in
    if supportsSSL(line) {
        validIPs += 1
    }
}
print(validIPs)
