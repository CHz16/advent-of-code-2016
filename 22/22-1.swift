#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "22.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)
let xCount = 33, yCount = 31


struct Node {
    let used, available: Int
}

func isViablePair(_ a: Node, _ b: Node) -> Bool {
    if a.used == 0 {
        return false
    }
    return (b.available >= a.used)
}


var grid: [[Node]] = Array(repeating: Array(repeating: Node(used: 0, available: 0), count: yCount), count: xCount)
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: " ")
    let x = Int(components[0])!
    let y = Int(components[1])!
    let used = Int(components[3])!
    let available = Int(components[4])!

    grid[x][y] = Node(used: used, available: available)
}

var viablePairs = 0
for x1 in 0..<xCount {
    for y1 in 0..<yCount {
        for x2 in 0..<xCount {
            for y2 in 0..<yCount {
                if x1 == x2 && y1 == y2 {
                    continue
                }

                if isViablePair(grid[x1][y1], grid[x2][y2]) {
                    viablePairs += 1
                }
            }
        }
    }
}
print(viablePairs)
