//: Playground - noun: a place where people can play

import Cocoa


// Part 2

func partTwo(_ n: Int) -> Int {
    if n == 1 {
        return 1
    }

    var base = 1
    while base < n {
        base *= 3
    }
    base /= 3

    let i = n - base - 1
    if i < base {
        return n - base
    } else {
        return base + 2 * (n - 2 * base)
    }
}

partTwo(3012210)