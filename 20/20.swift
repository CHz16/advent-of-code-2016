#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "20.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Parts 1 & 2

struct IPRange {
    let low, high: Int

    func contains(_ i: Int) -> Bool {
        return (i >= low && i <= high)
    }
}

var ranges: [IPRange] = []
input.enumerateLines { (line, stop) in
    let components = line.components(separatedBy: "-")
    ranges.append(IPRange(low: Int(components[0])!, high: Int(components[1])!))
}
ranges = ranges.sorted { $0.low < $1.low }

let maxIP = 4294967295

var currentIP = 0
var currentRange = 0
var validIPs = 0
var lowestIP: Int?
while currentIP <= maxIP {
    // If the currentIP isn't in the current blocked range, then add every IP from here to the start of the range to the count and move the current IP up to the start of the range. Also save the lowest such free IP we hit for part 1.
    if !ranges[currentRange].contains(currentIP) {
        if lowestIP == nil {
            lowestIP = currentIP
        }
        validIPs += ranges[currentRange].low - currentIP
        currentIP = ranges[currentRange].low
    }

    // Move the current IP to the end of the current blocked range.
    currentIP = ranges[currentRange].high + 1

    // Scan through the blocked ranges until we find the next one with an endpoint past the current IP. This check is necessary because ranges can overlap, so it's possible the next range in the list starts later than the last one we were considering but ends earlier.
    currentRange += 1
    while currentRange < ranges.count && ranges[currentRange].high < currentIP {
        currentRange += 1
    }

    // If there aren't any more ranges, then add every IP from here to the end to the count.
    if currentRange == ranges.count {
        validIPs += maxIP + 1 - currentIP
        break
    }
}
print(lowestIP!)
print(validIPs)
