//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "8", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


let width = 50
let height = 6

func pos(row: Int, col: Int) -> Int {
    return row * width + col
}

func printScreen(_ screen: [String]) {
    for row in 0..<height {
        for col in 0..<width {
            print(screen[pos(row: row, col: col)], terminator: "")
        }
        print("")
    }
}


// Parts 1 & 2

var screen = Array(repeating: " ", count: width * height)

input.enumerateLines { (line, stop) in
    let instruction = line.components(separatedBy: " ")

    if instruction[0] == "rect" {
        let args = instruction[1].components(separatedBy: "x")
        let width = Int(args[0])!
        let height = Int(args[1])!

        for row in 0..<height {
            for col in 0..<width {
                screen[pos(row: row, col: col)] = "#"
            }
        }
    } else {
        let offset = Int(instruction[4])!
        let index = Int(instruction[2].components(separatedBy: "=")[1])!

        if instruction[1] == "column" {
            for _ in 0..<offset {
                let temp = screen[pos(row: height - 1, col: index)]
                for row in (1..<height).reversed() {
                    screen[pos(row: row, col: index)] = screen[pos(row: row - 1, col: index)]
                }
                screen[pos(row: 0, col: index)] = temp
            }
        } else {
            for _ in 0..<offset {
                let temp = screen[pos(row: index, col: width - 1)]
                for col in (1..<width).reversed() {
                    screen[pos(row: index, col: col)] = screen[pos(row: index, col: col - 1)]
                }
                screen[pos(row: index, col: 0)] = temp
            }
        }
    }
}

printScreen(screen)
print(screen.map { $0 == "#" ? 1 : 0 }.reduce(0, +))