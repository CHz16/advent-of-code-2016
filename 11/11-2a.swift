#!/usr/bin/env xcrun swift


// Part 2

enum Thing: Equatable {
    case generator(String), microchip(String)
}

func ==(left: Thing, right: Thing) -> Bool {
    switch(left, right) {
    case let (.generator(a), .generator(b)) where a == b:
        return true
    case let (.microchip(a), .microchip(b)) where a == b:
        return true
    default:
        return false
    }
}

struct State {
    let floors: [[Thing]]
    let floor: Int

    var isWinState: Bool {
        for i in 0..<(floors.count - 1) {
            if !floors[i].isEmpty {
                return false
            }
        }
        return true
    }

    func validMoves() -> [State] {
        func isValidFloor(_ floor: [Thing]) -> Bool {
            var microchips: [String] = []
            var generators: [String] = []

            for thing in floor {
                switch thing {
                case let .microchip(id):
                    microchips.append(id)
                case let .generator(id):
                    generators.append(id)
                }
            }

            for microchip in microchips {
                if !generators.isEmpty && !generators.contains(microchip) {
                    return false
                }
            }
            return true
        }

        if floors[floor].isEmpty {
            return []
        }

        var combinations: [[Thing]] = []
        for thing in floors[floor] {
            combinations.append([thing])
        }
        for a in 0..<(floors[floor].count - 1) {
            for b in (a + 1)..<floors[floor].count {
                let elevator: [Thing] = [floors[floor][a], floors[floor][b]]
                combinations.append(elevator)
            }
        }

        var moves: [State] = []

        for elevator in combinations {
            let newCurrentFloor = floors[floor].filter { !elevator.contains($0) }
            if !isValidFloor(newCurrentFloor) {
                continue
            }

            // down
            if floor > 0 {
                var newFloors = floors
                newFloors[floor] = newCurrentFloor
                newFloors[floor - 1] += elevator

                if isValidFloor(newFloors[floor - 1]) {
                    moves.append(State(floors: newFloors, floor: floor - 1))
                }
            }

            // up
            if floor < (floors.count - 1) {
                var newFloors = floors
                newFloors[floor] = newCurrentFloor
                newFloors[floor + 1] += elevator

                if isValidFloor(newFloors[floor + 1]) {
                    moves.append(State(floors: newFloors, floor: floor + 1))
                }
            }
        }

        return moves
    }

    var string: String {
        var microchips: [String: Int] = [:]
        var generators: [String: Int] = [:]
        for (floor, things) in floors.enumerated() {
            for thing in things {
                switch thing {
                case let .microchip(id):
                    microchips[id] = floor
                case let .generator(id):
                    generators[id] = floor
                }
            }
        }

        var things: [(Int, Int)] = []
        for (id, floor) in microchips {
            things.append((floor, generators[id]!))
        }
        things = things.sorted {
            if $0.0 == $1.0 {
                return $0.1 < $1.1
            } else {
                return $0.0 < $1.0
            }
        }
        
        return "\(floor)" + things.description
    }
}


let initialState = State(floors: [
    [.generator("S"), .microchip("S"), .generator("P"), .microchip("P"), .generator("E"), .microchip("E"), .generator("D"), .microchip("D")],
    [.generator("T"), .generator("R"), .microchip("R"), .generator("C"), .microchip("C")],
    [.microchip("T")],
    []
    ], floor: 0)

var possibleStates = [initialState]
var visitedStateStrings = Set<String>()
visitedStateStrings.insert(initialState.string)

var moves = 0
bfs: while moves < 200 {
    print("\(moves) \(possibleStates.count)")
    moves += 1

    var newQueue: [State] = []
    for possibleState in possibleStates {
        let newStates = possibleState.validMoves().filter { visitedStateStrings.insert($0.string).0 }
        for newState in newStates {
            if newState.isWinState {
                print(moves)
                break bfs
            }
        }
        newQueue += newStates
    }
    
    possibleStates = newQueue
}
