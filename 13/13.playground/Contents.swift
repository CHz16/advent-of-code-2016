//: Playground - noun: a place where people can play

import Cocoa


let seed = 1352
let targetX = 31
let targetY = 39

var wallCache: [Int: [Int: Bool]] = [:]
func isWall(x: Int, y: Int) -> Bool {
    if let b = wallCache[x]?[y] {
        return b
    }

    var n = x * x + 3 * x + 2 * x * y + y + y * y + seed
    var bits = 0
    while n > 0 {
        if n % 2 == 1 {
            bits += 1
        }
        n /= 2
    }

    let b = (bits % 2 == 1)
    if wallCache[x] == nil {
        wallCache[x] = [:]
    }
    wallCache[x]?[y] = b
    return b
}


// Parts 1 & 2

var visited: [Int: [Int: Bool]] = [:]
var queue = [(1, 1)]
var steps = 0

bfs: while steps < 200 {
    var newQueue: [(Int, Int)] = []

    for (x, y) in queue {
        if x == targetX && y == targetY {
            print("steps to goal: \(steps)")
            break bfs
        }

        if x < 0 || y < 0 {
            continue
        }
        if visited[x]?[y] != nil {
            continue
        }
        if isWall(x: x, y: y) {
            continue
        }

        if visited[x] == nil {
            visited[x] = [:]
        }
        visited[x]?[y] = true

        newQueue += [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
    }

    if steps == 50 {
        let states = visited.values.map { $0.count }.reduce(0, +)
        print("states reachable in 50 steps: \(states)")
    }

    steps += 1
    queue = newQueue
}
