#!/usr/bin/env xcrun swift


let input = "10111100110001111"


func randomData(fromSeed seed: String, ofSize size: Int) -> String {
    if seed.count > size {
        let start = seed.startIndex
        let end = seed.index(start, offsetBy: size)
        return String(seed[start..<end])
    }

    var newString = seed
    newString.append("0")
    for c in seed.reversed() {
        if c == "0" {
            newString.append("1")
        } else {
            newString.append("0")
        }
    }
    return randomData(fromSeed: newString, ofSize: size)
}

func checksum(_ s: String) -> String {
    if s.count % 2 == 1 {
        return s
    }

    var newString = ""
    var cursor = s.startIndex
    while cursor < s.endIndex {
        if s[cursor] == s[s.index(after: cursor)] {
            newString.append("1")
        } else {
            newString.append("0")
        }
        cursor = s.index(cursor, offsetBy: 2)
    }
    return checksum(newString)
}


// Part 1
print(checksum(randomData(fromSeed: input, ofSize: 272)))

// Part 2
print(checksum(randomData(fromSeed: input, ofSize: 35651584)))
