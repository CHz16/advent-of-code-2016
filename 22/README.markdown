Day 22: Grid Computing
======================

http://adventofcode.com/2016/day/22

First part is just reading in data from a file and then comparing each pair, not too bad.

Second part is solving a sliding block puzzle in a huge grid. I wrote an A\* search routine, which solved the puzzle in 4 minutes and 47 seconds. It turns out the grid is super trivial and it would've been much much faster if I had just calculated it by hand! Oh well, I wrote A\* for the first time since college so good for me.

(The reason I didn't do it by hand is that I first tried to do it by hand, but I got a wrong answer because I printed out the grid super incorrectly while it was being read in instead of after it was fully compiled. However, I didn't realize right away that that's what I did, so I assumed the grid was correct and I was bad at counting. I noticed that the top row was completely clear of walls between the target data and goal, which is why I added severely penalized moving the target down in the cost estimate calculation. This might not have been a good idea if there were actually walls in that row! Luckily, in the correct grid, there also aren't any walls up there, so the optimal path does in fact never move the packet down.)

Storing the grid in every state is unnecessary and throws memory away; all we need to track is the free space and target data, and the walls can just be a constant array.

* Part 1: 173rd place (18:32)
* Part 2: 101st place (1:30:13)
