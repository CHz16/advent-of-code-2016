//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "9", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

func decompressedLength(_ input: String) -> Int {
    var l = input.count

    var cursor = input.startIndex
    while cursor < input.endIndex {
        if input[cursor] == "(" {
            var dataLength = 2
            var dataEnd = input.index(after: cursor)
            while input[dataEnd] != ")" {
                dataEnd = input.index(after: dataEnd)
                dataLength += 1
            }

            let args = input[input.index(after: cursor)..<dataEnd].components(separatedBy: "x")
            let len = Int(args[0])!
            let times = Int(args[1])!
            l += len * (times - 1) - dataLength

            cursor = input.index(dataEnd, offsetBy: len)
        }

        cursor = input.index(after: cursor)
    }

    return l
}

decompressedLength(input) - 1


// Part 2

func decompressedV2Length(_ input: String) -> Int {
    var l = 0

    var cursor = input.startIndex
    while cursor < input.endIndex {
        if input[cursor] == "(" {
            var dataLength = 2
            var dataEnd = input.index(after: cursor)
            while input[dataEnd] != ")" {
                dataEnd = input.index(after: dataEnd)
                dataLength += 1
            }

            let args = input[input.index(after: cursor)..<dataEnd].components(separatedBy: "x")
            let len = Int(args[0])!
            let times = Int(args[1])!

            let substrStart = input.index(after: dataEnd)
            let substrEnd = input.index(substrStart, offsetBy: len)

            l += times * decompressedV2Length(String(input[substrStart..<substrEnd]))
            cursor = substrEnd
        } else {
            l += 1
            cursor = input.index(after: cursor)
        }
    }

    return l
}

decompressedV2Length(input) - 1
