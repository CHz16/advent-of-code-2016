#!/usr/bin/env xcrun swift -F .. -framework CryptoSwift

import CryptoSwift


let seed = "gdjjyniy"


struct OpenPaths: OptionSet, Hashable {
    let rawValue: Int

    static let up = OpenPaths(rawValue: 1 << 0)
    static let down = OpenPaths(rawValue: 1 << 1)
    static let left = OpenPaths(rawValue: 1 << 2)
    static let right = OpenPaths(rawValue: 1 << 3)

    var hashValue: Int { return self.rawValue.hashValue }

    static func ==(left: OpenPaths, right: OpenPaths) -> Bool {
        return (left.rawValue == right.rawValue)
    }
}

struct State {
    let row, col: Int
    let path: String

    enum Direction { case up, down, left, right }
    func move(_ direction: Direction) -> State {
        switch direction {
        case .up:
            return State(row: row - 1, col: col, path: path + "U")
        case .down:
            return State(row: row + 1, col: col, path: path + "D")
        case .left:
            return State(row: row, col: col - 1, path: path + "L")
        case .right:
            return State(row: row, col: col + 1, path: path + "R")
        }
    }

    func openPaths() -> OpenPaths {
        func doorIsOpen(code: Character) -> Bool {
            return (code == "b" || code == "c" || code == "d" || code == "e" || code == "f")
        }

        let hash = (seed + path).md5()
        var openPaths = OpenPaths()
        if row != 0 && doorIsOpen(code: hash[hash.startIndex]) {
            openPaths.insert(.up)
        }
        if row != 3 && doorIsOpen(code: hash[hash.index(hash.startIndex, offsetBy: 1)]) {
            openPaths.insert(.down)
        }
        if col != 0 && doorIsOpen(code: hash[hash.index(hash.startIndex, offsetBy: 2)]) {
            openPaths.insert(.left)
        }
        if col != 3 && doorIsOpen(code: hash[hash.index(hash.startIndex, offsetBy: 3)]) {
            openPaths.insert(.right)
        }
        return openPaths
    }
}



// Part 1

var shortestPathQueue: [State] = [State(row: 0, col: 0, path: "")]
while !shortestPathQueue.isEmpty {
    let state = shortestPathQueue.removeFirst()
    let openPaths = state.openPaths()

    if state.row == 2 && state.col == 3 && openPaths.contains(.down) {
        print(state.move(.down).path)
        break
    }
    if state.row == 3 && state.col == 2 && openPaths.contains(.right) {
        print(state.move(.right).path)
        break
    }

    if openPaths.contains(.up) {
        shortestPathQueue.append(state.move(.up))
    }
    if openPaths.contains(.down) {
        shortestPathQueue.append(state.move(.down))
    }
    if openPaths.contains(.left) {
        shortestPathQueue.append(state.move(.left))
    }
    if openPaths.contains(.right) {
        shortestPathQueue.append(state.move(.right))
    }
}


// Part 2

var longestPath = State(row: 0, col: 0, path: "")

var longestPathQueue: [State] = [longestPath]
while !longestPathQueue.isEmpty {
    let state = longestPathQueue.removeFirst()
    var openPaths = state.openPaths()

    if state.row == 2 && state.col == 3 && openPaths.contains(.down) {
        longestPath = state.move(.down)
        openPaths.remove(.down)
    }
    if state.row == 3 && state.col == 2 && openPaths.contains(.right) {
        longestPath = state.move(.right)
        openPaths.remove(.right)
    }

    if openPaths.contains(.up) {
        longestPathQueue.append(state.move(.up))
    }
    if openPaths.contains(.down) {
        longestPathQueue.append(state.move(.down))
    }
    if openPaths.contains(.left) {
        longestPathQueue.append(state.move(.left))
    }
    if openPaths.contains(.right) {
        longestPathQueue.append(state.move(.right))
    }
}

print(longestPath.path.count, longestPath.path)
