Advent of Code 2016
===================

These are my solutions in Swift to [Advent of Code 2016](http://adventofcode.com/2016), a kind of fun series of 25 programming puzzles that ran in December 2016. I figured I'd throw these up on the web somewhere, because why not. If I was competing for a leaderboard spot that day (that is, I started coding at 9 PM Pacific), then the readme file for that day will give my time and place.

While the challenges had no time limit, there was a leaderboard that showed (essentially) the fastest 100 solvers of each day's problem, so naturally I tried to solve them as quickly as possible. So, most of the solutions here are simply what left my fingers the fastest. That's speedcoding for you.

I started writing every single challenge in a playground, because they're fantastic tools, but a lot of them I ended up running from the command line after finishing the algorithm because it was way faster. Usually this was because something was looping thousands of times and timelines collecting all those intermediate values massively slowed things down. So if you see a playground solution, that means it ran well enough that I didn't have to drop down and get a boost with `xcrun`.

Every user had their puzzle inputs randomly selected from a pool of possibilities, so the inputs you see in these files may not be the same ones you'd get were you to sign up.  I also tend to preprocess them in a text editor to make them way easier to parse.

A few of these solutions incorporate [Marcin Krzyżanowski's CryptoSwift framework](https://github.com/krzyzanowskim/CryptoSwift) for MD5 hashing. Every other bit of code in this repo is something I cranked out during this contest, though sometimes based heavily on something I've written before.

* Overall showing: 99th place (832 points)
* Best placement for any part: day 19, part 1 (7th place)
* Best placement for a part that I actually wrote code to solve: day 3, part 2 (32nd place)
* Special mention for the hybrid problem I wrote a little bit of code for, but mostly solved by debugging and hand calculation: day 25, part 1 (25th place)
* Best optimization: day 11
* Least ugly code: day 17

*See also: [2015](https://bitbucket.org/CHz16/advent-of-code-2015), [2017](https://bitbucket.org/CHz16/advent-of-code-2017), [2018](https://bitbucket.org/CHz16/advent-of-code-2018), [2019](https://bitbucket.org/CHz16/advent-of-code-2019), [2020](https://bitbucket.org/CHz16/advent-of-code-2020), [2021](https://bitbucket.org/CHz16/advent-of-code-2021), [2022](https://bitbucket.org/CHz16/advent-of-code-2022)*
