Day 14: One-Time Pad
====================

http://adventofcode.com/2016/day/14

Even more MD5 hashing. Part 1 is pretty boring, part 2 makes hashing much more expensive so you need to be smart about making sure you only hash each index once.

This took me a while because I missed the part of the instructions that says "Only consider the first such triplet in a hash." I found a logic for my incorrect code to produce the answer from the example input, and that same logic also produced the correct answer for the first part, adding to my confusion. It didn't work for the second half, though. 😩

My loop here is convoluted. The straightforward way would've been to cache the hashes and do something like this:

```
repeat until we've found 64 keys
    calculate the hash
    if the hash contains a triplet then
        calculate the next 1000 hashes
        if any of them contains the desired quintuplet then
            save this key
        end if
    end if
end repeat

the answer is the last key we found
```

Instead, I did it like this???

```
repeat until we've found at least 64 keys and there are no more candidates
    calculate the hash
    
    clear out any candidates that we added more than 1000 keys ago
    if this hash contains the quintuplet of any candidate then
        save the key of that candidate
    end if
    
    if we've found less than 64 valid keys and the hash contains a triplet then
        save this key and the desired quintuplet as a candidate
    end if
end repeat

sort the list of keys from lowest to highest
the answer is the 64th key
```

Which I guess is cool because it avoids needing a cache altogether, but it's a really weird flow and I don't know why it was my first thought.

* Part 1: 122nd place (28:54)
* Part 2: 125th place (47:41)
