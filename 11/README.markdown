Day 11: Radioisotope Thermoelectric Generators
==============================================

http://adventofcode.com/2016/day/11

HELL BEGINS TODAY

Two different files for part 1 and part 2; they only differ in the initial input given. Part 1 finishes in a few minutes, but part 2 takes a bit under two hours.

Did this with a fairly unoptimized breadth-first search with only basic culling for invalid or already visited states. I have a few ideas for optimizations, chiefly being throwing out states that are isomorphic¹ to ones we've visited already, that I'll put in separate files if I get around to them and they work.

1: "Isomorphic" as in: chip and generator pairs are only distinguished by their identifiers and are interchangeable in that regard, so two states are really the same state if you can change the identifiers of one and get the other. For example, these two states are isomorphic, because you can swap A & B:

```
F4 .  .  .  .  .      F4 .  .  .  .  .
F3 .  .  .  BG .      F3 .  AG .  .  .
F2 .  AG .  .  .      F2 .  .  .  BG .
F1 E  .  AM .  BM     F1 E  .  AM .  BM
```

* Part 1: 82nd place (1:31:36)
* Part 2: 113th place (3:20:30)

Update: I DID THAT ONE OPTIMIZATION AND NOW IT FINISHES PART 2 IN TWENTY-SEVEN SECONDS WOW. See `11-1a.swift` and `11-2a.swift` for the alternate version.
