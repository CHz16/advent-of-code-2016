Day 12: Leonardo's Monorail
===========================

http://adventofcode.com/2016/day/12

Another fake VM, like day 23 of last year. Having done basically the same thing before, I just cranked something about pretty quick... and didn't place. Competition is cutthroat this year. 😩

* Part 1: 149th place (20:41)
* Part 2: 142nd place (22:26)
