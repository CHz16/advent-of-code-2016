Day 9: Explosives in Cyberspace
===============================

http://adventofcode.com/2016/day/9

Calculate the length of the decompressed version of a string compressed by a simple algorithm. The first part was relatively simple, but I got the wrong answer because I hardcoded a length of 5 for a data marker in the `l += len * (times - 1) - dataLength` calculation; all of the data markers in the test cases are length 5, but if either of the arguments contains more than two digits, then obviously it will be longer than 5. If I hadn't messed that up, I probably would've cracked the leaderboard, oops!

Part 2 is basically the same problem, except data markers can multiply data markers so you need to use recursion.

* Part 1: 222nd place (22:57)
* Part 2: 102nd place (33:21)
