Day 23: Safe Cracking
=====================

http://adventofcode.com/2016/day/23

Part 1 is to add one more instruction to the VM from day 12. Toggle changes an instruction to a different one with the same argument(s). This new instruction could possibly be invalid, which was a pain to add to the VM because it uses type safety to enforce validity. I ended up having to tear that out and add manual checks everywhere, which was a pain in the ass.

The second part increases an input value and gives you a strong hint that this will never stop executing because it's trying to multiply large numbers with just increment instructions. So, I found the main multiplication loop (by experimentation mostly, cutting off the assembly at certain points and inspecting registers), converted it into a multiply instruction, and then added that multiply instruction to the VM.

I also added a noop instruction, so that instead of replacing the whole multiplication loop with just one instruction, I could pad it with noops to keep the code length the same. Did that because I wasn't sure if the code was jumping into the middle of that loop or modifying any of it. Did get the right answer, so that was nice.

* Part 1: 151st place (35:31)
* Part 2: 94th place (53:48)
