#!/usr/bin/env xcrun swift


let seed = "......^.^^.....^^^^^^^^^...^.^..^^.^^^..^.^..^.^^^.^^^^..^^.^.^.....^^^^^..^..^^^..^^.^.^..^^..^^^.."
let seedArray = seed.map { $0 == "." ? 1 : 0 }

func safeTiles(row: [Int], numberOfRows: Int) -> Int {
    var safeCount = 0
    var row = row
    for _ in 0..<numberOfRows {
        safeCount += row.reduce(0, +)

        row = [1] + row + [1]
        var nextRow: [Int] = []
        for i in 1..<(row.count - 1) {
            nextRow.append(row[i - 1] == row[i + 1] ? 1 : 0)
        }
        row = nextRow
    }

    return safeCount
}

// Parts 1 & 2
print(safeTiles(row: seedArray, numberOfRows: 40))
print(safeTiles(row: seedArray, numberOfRows: 400000))
