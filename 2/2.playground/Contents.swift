//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "2", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Part 1

var row = 1, col = 1
var code = ""

input.enumerateLines { (line, stop) in
    for direction in line {
        var rowDelta = 0, colDelta = 0
        if direction == "U" {
            rowDelta = -1
        } else if direction == "D" {
            rowDelta = 1
        } else if direction == "L" {
            colDelta = -1
        } else if direction == "R" {
            colDelta = 1
        }

        row = max(0, min(2, row + rowDelta))
        col = max(0, min(2, col + colDelta))
    }

    code += String(1 + row * 3 + col)
}

code


// Part 2

let buttons = ["", "", "1", "", "", "", "2", "3", "4", "", "5", "6", "7", "8", "9", "", "A", "B", "C", "", "", "", "D", "", ""]

row = 2
col = 0
code = ""

input.enumerateLines { (line, stop) in
    for direction in line {
        var rowDelta = 0, colDelta = 0
        if direction == "U" {
            rowDelta = -1
        } else if direction == "D" {
            rowDelta = 1
        } else if direction == "L" {
            colDelta = -1
        } else if direction == "R" {
            colDelta = 1
        }

        let newRow = max(0, min(4, row + rowDelta))
        let newCol = max(0, min(4, col + colDelta))
        if buttons[newRow * 5 + newCol] != "" {
            row = newRow
            col = newCol
        }
    }

    code += buttons[row * 5 + col]
}

code
