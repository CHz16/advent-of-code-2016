Day 6: Signals and Noise
========================

http://adventofcode.com/2016/day/6

Just counting the occurrences of characters that appear in the first character of a list of strings, the second character, and so on.

* Part 1: 193rd place (7:16)
* Part 2: 164th place (8:03)
