//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "10", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

enum OutputDestination {
    case bot(Int), output(Int)
}

var bots: [Int: [Int]] = [:]
var outputInstructions: [Int: (low: OutputDestination, high: OutputDestination)] = [:]
var compareQueue: [Int] = []

// Read in initial values and output instructions
input.enumerateLines { (line, stop) in
    let words = line.components(separatedBy: " ")

    if words[0] == "value" {
        // Store a chip with a bot
        let bot = Int(words[5])!
        let value = Int(words[1])!
        var d = bots[bot] ?? []
        d.append(value)
        bots[bot] = d

        if d.count == 2 {
            compareQueue.append(bot)
        }
    } else {
        // Store the instruction for which places a bot is to give its chips
        let holder = Int(words[1])!

        let id1 = Int(words[6])!
        let dest1: OutputDestination
        if words[5] == "bot" {
            dest1 = .bot(id1)
        } else {
            dest1 = .output(id1)
        }

        let id2 = Int(words[11])!
        let dest2: OutputDestination
        if words[10] == "bot" {
            dest2 = .bot(id2)
        } else {
            dest2 = .output(id2)
        }

        outputInstructions[holder] = (dest1, dest2)
    }
}


var outputs: [Int: Int] = [:]

// While there's a bot with two chips, pass out its chips to its recipients.
while !compareQueue.isEmpty {
    let holder = compareQueue.removeFirst()
    var chips = bots[holder]!
    bots[holder] = []

    // Make sure the chips are ordered low first, high second
    if chips[0] > chips[1] {
        (chips[0], chips[1]) = (chips[1], chips[0])
    }

    guard let instructions = outputInstructions[holder] else {
        continue
    }

    // Low
    switch instructions.0 {
    case let .bot(target):
        var d = bots[target] ?? []
        d.append(chips[0])
        bots[target] = d

        if d.count == 2 {
            compareQueue.append(target)
        }

        // Part 1 check
        if d == [17, 61] || d == [61, 17] {
            print(target)
        }
    case let .output(target):
        outputs[target] = chips[0]
    }

    // High
    switch instructions.1 {
    case let .bot(target):
        var d = bots[target] ?? []
        d.append(chips[1])
        bots[target] = d

        if d.count == 2 {
            compareQueue.append(target)
        }

        // Part 1 check
        if d == [17, 61] || d == [61, 17] {
            print(target)
        }
    case let .output(target):
        outputs[target] = chips[1]
    }
}

// Part 2 calculation -- the compiler didn't like the line outputs[0]! * outputs[1]! * outputs[2]! for some reason, so we'll just do it this way
var part2 = outputs[0]!
part2 *= outputs[1]!
part2 *= outputs[2]!
print(part2)
