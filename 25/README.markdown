Day 25: Clock Signal
====================

http://adventofcode.com/2016/day/15

Two part problem. The first is just to add an output instruction to the VM from day 23.

The second part is to find the lowest number to set the `a` register to to cause the input code to output 0 & 1 alternating forever. I did this without programming by observation: I set `a` to various values and had the VM print out the registers as well whenever it output anything, and it became clear that the program was storing `a` + 4 * 633 into the `d` register and then repeatedly printing out the binary representation of it backwards forever. So, the puzzle was just to calculate the lowest number to add to 4 * 633 such that the result in binary is 1010...10.

* Part 1: 25th place (8:54)
* Part 2: 19th place (9:05)
