#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "21.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


func scramble(_ i: String) -> String {
    var password = i

    input.enumerateLines { (line, stop) in
        let components = line.components(separatedBy: " ")

        if components[0] == "swap" && components[1] == "position" {
            let aIndex = password.index(password.startIndex, offsetBy: Int(components[2])!)
            let a = String(password[aIndex])
            let bIndex = password.index(password.startIndex, offsetBy: Int(components[5])!)
            let b = String(password[bIndex])

            password.replaceSubrange(aIndex..<password.index(after:aIndex), with: b)
            password.replaceSubrange(bIndex..<password.index(after:bIndex), with: a)
        } else if components[0] == "swap" && components[1] == "letter" {
            let a = components[2]
            let b = components[5]

            password = password.replacingOccurrences(of: a, with: "<")
            password = password.replacingOccurrences(of: b, with: a)
            password = password.replacingOccurrences(of: "<", with: b)
        } else if components[0] == "reverse" {
            let aIndex = password.index(password.startIndex, offsetBy: Int(components[2])!)
            let bIndex = password.index(password.startIndex, offsetBy: Int(components[4])!)

            password.replaceSubrange(aIndex...bIndex, with: password[aIndex...bIndex].reversed())
        } else if components[0] == "rotate" && components[1] == "left" {
            let distance = Int(components[2])!

            password = String(password.dropFirst(distance)) + password[password.startIndex..<password.index(password.startIndex, offsetBy: distance)]
        } else if components[0] == "rotate" && components[1] == "right" {
            let distance = Int(components[2])!

            password = password[password.index(password.endIndex, offsetBy: -distance)..<password.endIndex] + String(password.dropLast(distance))
        } else if components[0] == "rotate" && components[1] == "based" {
            var distance = 0
            while password[password.index(password.startIndex, offsetBy: distance)] != components[6][components[6].startIndex] {
                distance += 1
            }
            distance += (distance >= 4) ? 2 : 1
            distance %= password.count

            password = password[password.index(password.endIndex, offsetBy: -distance)..<password.endIndex] + String(password.dropLast(distance))
        } else if components[0] == "move" {
            let aIndex = password.index(password.startIndex, offsetBy: Int(components[2])!)
            let a = password[aIndex]
            let bIndex = password.index(password.startIndex, offsetBy: Int(components[5])!)
            
            password.remove(at: aIndex)
            password.insert(a, at: bIndex)
        }
    }

    return password
}


// Part 1

print(scramble("abcdefgh"))


// Part 2

func permutations(_ chars: [String]) -> [String] {
    if chars.count == 1 {
        return chars
    }

    var s: [String] = []
    for c in chars {
        s += permutations(chars.filter { $0 != c }).map { c + $0 }
    }
    return s
}

for p in permutations(["a", "b", "c", "d", "e", "f", "g", "h"]) {
    if scramble(p) == "fbgdceah" {
        print(p)
        break
    }
}
