//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "4", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


// Parts 1 & 2

var validIdSum = 0

input.enumerateLines { (line, stop) in
    // Get the sector ID first, because we use it in decryption.
    let idStart = line.index(line.endIndex, offsetBy: -10)
    let idEnd = line.index(idStart, offsetBy: 3)
    let id = Int(line[idStart..<idEnd])!

    // Count the occurrences of characters in the encrypted room name and decrypt it simultaneously.
    var decryptedName = ""
    var chars: [Character: Int] = [:]
    for char in line {
        if char == "-" {
            decryptedName += " "
            continue
        }
        if let _ = Int(String(char)) {
            break
        }

        let prevCount = chars[char] ?? 0
        chars[char] = prevCount + 1

        let asciiValue = Int(String(char).unicodeScalars.first!.value)
        let decryptedValue = (asciiValue - 97 + id) % 26 + 97
        decryptedName += String(Character(UnicodeScalar(decryptedValue)!))
    }

    // Calculate the correct checksum of the encrypted room name.
    let sortedChars = chars.sorted {
        if $0.value == $1.value {
            return $0.key < $1.key
        } else {
            return $0.value > $1.value
        }
    }
    var correctChecksum = ""
    for i in 0..<5 {
        correctChecksum += String(sortedChars[i].key)
    }

    // Check that the checksum of the input is valid, and if so, add the ID to the total and print out the name & ID for part 2.
    let checksumStart = line.index(line.endIndex, offsetBy: -6)
    let checksumEnd = line.index(checksumStart, offsetBy: 5)
    let checksum = line[checksumStart..<checksumEnd]
    if correctChecksum != checksum {
        return
    }
    validIdSum += id
    print(decryptedName + String(id))
}

validIdSum
