Day 4: Security Through Obscurity
=================================

http://adventofcode.com/2016/day/4

Pretty straightforward string processing problems. Swift's Extremely Good Unicode Handling makes things a little harder than they'd be in other languages where you can just cast a character to a integer and boom, but whatevs.

This solution dumps all of the decrypted room names for part 2 to the console, because I had no idea if the desired output would literally be something similar to "room where North Pole objects are stored" or if it'd be more indirectly about "toys" or "presents" or what have you. So I just copied all the output into a text editor and searched various guesses until something promising showed up.
