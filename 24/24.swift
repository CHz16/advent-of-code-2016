#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "24.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


// Read in the maze data

var convert: [Character: Int] = ["0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7]

var points: [Int: (Int, Int)] = [:]
var walls: [[Bool]] = []

var y = 0
input.enumerateLines { (line, stop) in
    var row: [Bool] = []
    var x = 0
    for c in line {
        if let i = convert[c] {
            points[i] = (x, y)
            row.append(false)
        } else if c == "." {
            row.append(false)
        } else {
            row.append(true)
        }
        x += 1
    }
    walls.append(row)
    y += 1
}


// BFS out from every point to find the shortest distance to every other point.

struct State {
    let x, y, steps: Int
}

var pairwiseDistances: [Int: [Int: Int]] = [:]
for a in points.keys {
    var visited = Array(repeating: Array(repeating: false, count: walls[0].count), count: walls.count)
    var queue: [State] = [State(x: points[a]!.0, y: points[a]!.1, steps: 0)]
    // We want to keep the BFS going until we've calculated the distance to every other point, so the loop here runs until pairwiseDistances[a] is the same length as the number of points.
    while pairwiseDistances[a] == nil || pairwiseDistances[a]!.count < points.keys.count {
        let state = queue.removeFirst()
        if visited[state.y][state.x] {
            continue
        }
        visited[state.y][state.x] = true

        for (b, bCoords) in points {
            if state.x == bCoords.0 && state.y == bCoords.1 {
                let pairs = [(a, b), (b, a)]
                for (a, b) in pairs {
                    if pairwiseDistances[a] == nil {
                        pairwiseDistances[a] = [:]
                    }
                    pairwiseDistances[a]?[b] = state.steps
                }
            }
        }

        let newPoints = [(state.x + 1, state.y), (state.x - 1, state.y), (state.x, state.y + 1), (state.x, state.y - 1)]
        for (newX, newY) in newPoints {
            if walls[newY][newX] {
                continue
            }
            queue.append(State(x: newX, y: newY, steps: state.steps + 1))
        }
    }
}


// Permutation generator: the easiest strategy here is just to generate every single possible path and calculate all their distances.

func permutations(_ nums: [Int]) -> [[Int]] {
    if nums.count == 1 {
        return [nums]
    }

    var s: [[Int]] = []
    for n in nums {
        s += permutations(nums.filter { $0 != n }).map { [n] + $0 }
    }
    return s
}


// Part 1

var shortestDistance = Int.max
for path in permutations(Array(points.keys.filter { $0 != 0 })) {
    let fullPath = [0] + path
    var distance = 0
    var i = 0
    while i < fullPath.count - 1 {
        distance += pairwiseDistances[fullPath[i]]![fullPath[i + 1]]!
        i += 1
    }

    if distance < shortestDistance {
        print(distance, fullPath)
        shortestDistance = distance
    }
}


// Part 2

shortestDistance = Int.max
print("")
for path in permutations(Array(points.keys.filter { $0 != 0 })) {
    let fullPath = [0] + path + [0]
    var distance = 0
    var i = 0
    while i < fullPath.count - 1 {
        distance += pairwiseDistances[fullPath[i]]![fullPath[i + 1]]!
        i += 1
    }

    if distance < shortestDistance {
        print(distance, fullPath)
        shortestDistance = distance
    }
}
