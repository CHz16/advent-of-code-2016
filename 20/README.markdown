Day 20: Firewall Rules
======================

http://adventofcode.com/2016/day/20

Given a large range and a bunch of subranges, first find the lowest number in the range that doesn't fall into any subrange, then find how many numbers total don't fall into any subrange.

The range is [0, 2^32] so iteration over the whole thing isn't a particularly good idea. Instead, we sort the intervals by their starting point and then do some smart iteration over that list, so the runtime depends only on the number of subranges.

* Part 1: 213th place (20:38)
* Part 2: 162nd place (30:51)
