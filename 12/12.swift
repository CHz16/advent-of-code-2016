#!/usr/bin/env xcrun swift

import Cocoa

let url = URL(fileURLWithPath: "12.txt")
let input = try String(contentsOf: url, encoding: String.Encoding.utf8)


typealias Register = String
var registers = ["a": 0, "b": 0, "c": 0, "d": 0]

enum Value {
    case literal(Int), register(Register)

    init(_ x: String) {
        if let x = Int(x) {
            self = .literal(x)
        } else {
            self = .register(x)
        }
    }

    var v: Int {
        switch self {
        case let .literal(n):
            return n
        case let .register(r):
            return registers[r]!
        }
    }
}

enum Instruction {
    case copy(Value, Register), increment(Register), decrement(Register), jumpNotZero(Value, Value)
}

var code: [Instruction] = []
input.enumerateLines { (line, stop) in
    let tokens = line.components(separatedBy: " ")
    switch tokens[0] {
    case "cpy":
        code.append(.copy(Value(tokens[1]), tokens[2]))
    case "inc":
        code.append(.increment(tokens[1]))
    case "dec":
        code.append(.decrement(tokens[1]))
    default:
        code.append(.jumpNotZero(Value(tokens[1]), Value(tokens[2])))
    }
}


// Part 1

var pc = 0
while pc < code.count {
    let instruction = code[pc]
    pc += 1

    switch instruction {
    case let .copy(value, destination):
        registers[destination] = value.v
    case let .increment(register):
        registers[register] = registers[register]! + 1
    case let .decrement(register):
        registers[register] = registers[register]! - 1
    case let .jumpNotZero(value, offset):
        if value.v != 0 {
            pc += offset.v - 1
        }
    }
}

print(registers)


// Part 2

registers = ["a": 0, "b": 0, "c": 1, "d": 0]

pc = 0
while pc < code.count {
    let instruction = code[pc]
    pc += 1

    switch instruction {
    case let .copy(value, destination):
        registers[destination] = value.v
    case let .increment(register):
        registers[register] = registers[register]! + 1
    case let .decrement(register):
        registers[register] = registers[register]! - 1
    case let .jumpNotZero(value, offset):
        if value.v != 0 {
            pc += offset.v - 1
        }
    }
}

print(registers)
