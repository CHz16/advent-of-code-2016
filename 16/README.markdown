Day 16: Dragon Checksum
=======================

http://adventofcode.com/2016/day/16

Some more string manipulation. Part 2 explosively ups the size, but not enough to stop it from being brute forceable; the program finishes in two and a half minutes. :<

I realized in retrospect that this really isn't a string manipulation problem, but rather just a sequence manipulation problem, so this might run better if I use just an array of booleans rather than a string. I just slapped a quick version of that together and it runs about a minute faster, so that's keen.
