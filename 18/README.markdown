Day 18: Like a Rogue
====================

http://adventofcode.com/2016/day/18

Just counting active states in an [elementary cellular automaton](https://en.wikipedia.org/wiki/Elementary_cellular_automaton), not too hard at all.
