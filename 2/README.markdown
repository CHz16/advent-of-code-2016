Day 2: Bathroom Security
========================

http://adventofcode.com/2016/day/2

Follow movement instructions on a keypad to generate a password. Code is basically the same for both parts, save for an extra validation check in the second part for the more complicated button layout.
