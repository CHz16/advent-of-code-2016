Day 24: Air Duct Spelunking
===========================

http://adventofcode.com/2016/day/24

Did this one by running a breadth-first search from every single point in the maze to calculate the shortest distance to every other point and then permuting every single possible path between the points to find the shortest overall path. This strategy works because we can revisit points; it would be a lot trickier if we couldn't.

The second part adds a return to 0 at the end of every single path and so was a trivial modification.

* Part 1: 90th place (44:52)
* Part 2: 80th place (45:22)
