//: Playground - noun: a place where people can play

import Cocoa

let url = Bundle.main.url(forResource: "3", withExtension: "txt")
let input = try String(contentsOf: url!, encoding: String.Encoding.utf8)


func isValidTriangle(_ x: Int, _ y: Int, _ z: Int) -> Bool {
    return (x + y) > z && (y + z) > x && (z + x) > y
}


// Part 1

var validTriangleCount = 0

input.enumerateLines { (triple, stop) in
    let nums = triple.components(separatedBy: " ")
    let x = Int(nums[0])!
    let y = Int(nums[1])!
    let z = Int(nums[2])!

    if isValidTriangle(x, y, z) {
        validTriangleCount += 1
    }
}

validTriangleCount


// Part 2

validTriangleCount = 0
var t1: [Int] = []
var t2: [Int] = []
var t3: [Int] = []

input.enumerateLines { (row, stop) in
    let nums = row.components(separatedBy: " ")
    t1.append(Int(nums[0])!)
    t2.append(Int(nums[1])!)
    t3.append(Int(nums[2])!)

    if t1.count == 3 {
        if isValidTriangle(t1[0], t1[1], t1[2]) {
            validTriangleCount += 1
        }
        if isValidTriangle(t2[0], t2[1], t2[2]) {
            validTriangleCount += 1
        }
        if isValidTriangle(t3[0], t3[1], t3[2]) {
            validTriangleCount += 1
        }

        t1 = []
        t2 = []
        t3 = []
    }
}

validTriangleCount