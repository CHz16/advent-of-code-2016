Day 15: Timing is Everything
============================

http://adventofcode.com/2016/day/15

So I didn't write any code for today's problem? Because both parts were literally just "apply the Chinese Remainder Theorem"?? And I just found [a solver online](http://davidwees.com/chineseremaindertheorem/)??? And probably got my best results of the contest so far because of it????

* Part 1: 26th place (6:46)
* Part 2: 22nd place (7:24)
