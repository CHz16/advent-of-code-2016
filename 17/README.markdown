Day 17: Two Steps Forward
=========================

http://adventofcode.com/2016/day/17

Combining two things we've done several times already, MD5 hashing and breadth-first search! This problem defies caching¹ due to the door states depending on your entire path, but the search space is small enough that enumerating isn't a big deal.

I think I spent way too much time looking up docs (looking at you, OptionSet) and engineering than actually programming so I didn't place that well, but at least those search loops read super cleanly??????

1: I didn't realize this at first and initially did implement a cache that used an OpenPaths as a dictionary key, which is why that struct unnecessarily conforms to Hashable. I forgot to delete that code before committing so whatevs

* Part 1: 172nd place (30:34)
* Part 2: 162nd place (42:58)
