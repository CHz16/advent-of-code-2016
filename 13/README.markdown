Day 13: A Maze of Twisty Little Cubicles
========================================

http://adventofcode.com/2016/day/13

Generate a maze according to a mathematical formula, then solve it. Tailor-made for breath-first search, not too bad at all. The second part is to count the number of spaces visitable from the starting position within 50 steps; since I stored the visited spaces in a 2D dictionary in part 1, this ended up being as easy as just summing the counts of each row dictionary.

I got the wrong answer in part 2, though, because of a logic error! I originally had the bounds check as `x < 0 && y < 0` instead of `x < 0 || y < 0`, which allows states where one coordinate is negative. This didn't affect the part 1 answer, but it did add 30 invalid states to the visit count.

* Part 1: 37th place (14:28)
* Part 2: 40th place (20:28)
