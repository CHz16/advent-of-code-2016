#!/usr/bin/env xcrun swift -F .. -framework CryptoSwift

import CryptoSwift

let input = "uqwqemis"


// Part 1

var n = 0
var password = ""

trial: while true {
    let trialString = input + String(n)
    let hash = trialString.md5()

    var c = 0
    for char in hash {
        if c == 5 {
            print(n, hash)
            password += String(char)
            if password.count == 8 {
                break trial
            }
            break
        }

        if char != "0" {
            break
        }

        c += 1
    }

    n += 1
}

print(password)
