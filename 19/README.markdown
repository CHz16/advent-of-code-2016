Day 19: An Elephant Named Joseph
================================

http://adventofcode.com/2016/day/19

Part 1 is just the classic Josephus problem. Turns out this is astoundingly easy to solve quickly: [all you have to do is rotate the number of people left by one bit](https://www.youtube.com/watch?v=uCsD3ZGzMgE). So... I got seventh place for knowing that.

Then I failed to place on part 2 because that one I actually had to code, and first I made a few mistakes, and then the solution I came up with involved removing elements from an Array one at a time, and that turns out to be astoundingly slow when it has three million elements? I honestly don't really know if there's a better built-in data structure that would've performed better while allowing random access.

So anyway, after an hour and a half, I thought, "hey, the outputs for the normal Josephus problem follow a pattern, maybe the ones for this modified one do too?" And what would you know, they totally do:

```
01: 1
02: 1
03: 3
04: 1
05: 2
06: 3
07: 5
08: 7
09: 9
10: 1
11: 2
12: 3
13: 4
14: 5
15: 6
16: 7
17: 8
18: 9
19: 11
20: 13
21: 15
22: 17
23: 19
24: 21
25: 23
26: 25
27: 27
28: 1
```

Every time your index hits a power of 3, you reset the answer to 1 for the next index, and then count up by one for every index after that until the answer is that power of 3 again. Then you count up by two for every index until the index hits the next power of 3, after which you reset again.

Solved this problem incredibly quickly after finding that pattern. Really should've thought of checking that sooner. 😩😩😩

* Part 1: 7th place (3:22)
* Part 2: 186th place (1:55:24)
