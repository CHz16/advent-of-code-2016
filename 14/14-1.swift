#!/usr/bin/env xcrun swift -F .. -framework CryptoSwift

import CryptoSwift


let salt = "yjdafjpo"


// Part 1

var candidates: [(Int, String)] = []
var hitTarget = false
var keyIndices = Set<Int>()

var n = 0
trial: while true {
    let trialString = salt + String(n)
    let hash = trialString.md5()

    candidates = candidates.filter { n <= ($0.0 + 1000) }
    if hitTarget && candidates.isEmpty {
        break
    }
    for (candidateN, s) in candidates {
        if hash.contains(s) {
            keyIndices.insert(candidateN)
            if keyIndices.count == 64 {
                hitTarget = true
            }
        }
    }

    if hitTarget {
        n += 1
        continue
    }

    var cursor = hash.startIndex
    let endIndex = hash.index(hash.endIndex, offsetBy: -2)
    while cursor != endIndex {
        let a = hash[cursor]
        let b = hash[hash.index(cursor, offsetBy: 1)]
        let c = hash[hash.index(cursor, offsetBy: 2)]

        if a == b && a == c {
            candidates.append((n, String(repeating: String(a), count: 5)))
            break
        }

        cursor = hash.index(after: cursor)
    }

    n += 1
}

print(keyIndices.sorted()[63])
