Day 7: Internet Protocol Version 7
==================================

http://adventofcode.com/2016/day/7

Pretty simple string pattern searching, nothing too heady. Here's a straightforward and unoptimized solution, the data set wasn't so large that efficiency was super important.

The only "trick" is splitting the addresses on both left and right square brackets in the loops and using modulo math; because of the string format, every odd sequence in that split list will be a hypernet sequence, while every even sequence won't.
