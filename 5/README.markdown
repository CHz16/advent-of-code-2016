Day 5: How About a Nice Game of Chess?
======================================

http://adventofcode.com/2016/day/5

Calculate millions of MD5 hashes, because why not. Basically the same problem as [day 4 of last year](https://bitbucket.org/CHz16/advent-of-code-2015/src/5213beed350ebac2ae840df204dc12c93367be29/4/) with an extra couple of twists.

CommonCrypto still isn't exposed to Swift in the year of our lord 2016, so last year I did probably the worst possible solution: NSTask out to `/sbin/md5`. Don't do that.

This year I just grabbed [Marcin Krzyżanowski's CryptoSwift framework](https://github.com/krzyzanowskim/CryptoSwift) and imported it into Swift command-line script. Which was a much better idea because I ended up having to calculate more than twenty million hashes for my input.

`5-1.swift` runs the first part of the problem and `5-2.swift` runs the second. `5-2.swift` is prepopulated with some data calculated from the first half of the problem, so it won't work with a different input without completely initializing `n`, `count`, `password`, and `lookup`.

* Part 1: 412th place (26:59, plugging in CryptoSwift took a while)
* Part 2: 401st place (44:15)
