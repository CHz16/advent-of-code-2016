Day 8: Two-Factor Authentication
================================

http://adventofcode.com/2016/day/8

Parse a list of instructions for how to modify a 2D array. I feel like I really should be writing more about these problems but every day I basically have the exact same comment, which is "pretty straightforward problem, just hammered a solution in place without algorithmic trickery."

* Part 1: 66th place (16:53)
* Part 2: 48th place (17:10)
