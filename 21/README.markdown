Day 21: Scrambled Letters and Hash
==================================

http://adventofcode.com/2016/day/21

More string manipulation. The kinds of which are not especially easy with Swift strings. So this took a while and is pretty ugly!

In part 2, the operations can vary based on the input string because of the `rotate based` operation; you can't¹ just find the transformed indices of one scrambled string and reverse those to untransform a different string. So I just brute forced it haha, 8! is not a large search space.

1: Actually on reflection, this **is** a reversible transformation with the input string length of 8! For other string lengths it's definitely not.

* Part 1: 184th place (44:46)
* Part 2: 162nd place (1:06:49)
