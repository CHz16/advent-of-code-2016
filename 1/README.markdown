Day 1: No Time for a Taxicab
============================

http://adventofcode.com/2016/day/1

First part: given a list of directions in a city (turn right and walk 2 blocks, turn left and walk 3 blocks), calculate the Manhattan distance between the start and endpoint if you followed the directions.

Second part: calculate the Manhattan distance between the start and the first block you visit twice by following the directions. Visiting in the **middle** of an instruction counts, so for example, `R8, R4, R4, R8`:

        •
        |
        |
        |
    S>--+---•
        |   v
        |   |
        ^   |
        •--<•

Given S is (0, 0), the answer here is at (4, 0), even though the final `R8` instruction continues past it.

No tricks in the code here, except maybe the `(1...distance).map` thing in part 2 to generate the list of points traversed in a movement step. The `Heading` enum is definitely overkill for this.
